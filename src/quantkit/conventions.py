"""Conventions module."""
import numpy as np
import pandas as pd
from typing import Union


ArrayLike = Union[np.array, pd.Series, pd.DataFrame]


BYEAR = 261
