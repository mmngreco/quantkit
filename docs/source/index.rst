Welcome to quantkit's documentation!
====================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   install.rst
   quickstart.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
