## Description

- closes #xy
- related #xy

## Tasks

- [ ] included tests (new test)
- [ ] all tests passed (`pytest test`)
- [ ] check flake8 complains (`flake8 src` and `black src -l79`)
- [ ] docstring added (`pydocstyle src`)
